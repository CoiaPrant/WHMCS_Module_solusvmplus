## 特别注意

- 自选端口需要配合 SolusVMNAT 使用
- 基于 solusvmpro 二次开发
- 如果面板没有 HTTPS 证书，则图片可能会被浏览器拦截

## 使用方法

- 把插件目录直接扔到 WHMCS/modules/servers 下即可
- 自定义字段 vserverid (文本框 仅管理员可见) rootpassword (文本框 仅管理员可见)

## 特性

- 支持 NAT VPS
- 支持 WHMCS 8.0
- TUN/TAP 开关
- 默认主备一体模式，自动显示 NAT IP
- 支持多服务器
- 支持显示面板 IP

## 赞助

- USDT TRC20 地址 `TNU2wK4yieGCWUxezgpZhwMHmLnRnXRtmu`
